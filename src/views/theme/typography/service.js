import axios from 'axios'

const API_URL = `https://anhkk.site:6060`
const API_URL_PRODUCTS = `https://anhkk.site:6060/products`
const accessToken = localStorage.getItem('accessToken')

export const getAllCategories = (accessToken) => {
  const config = {
    headers: { Authorization: `Bearer ${accessToken}` },
  }
  var url = 'https://anhkk.site:6060/category'
  return axios.get(url, config)
}

export const getProductByCate = (id) => {
  var url = 'https://anhkk.site:6060/categories/id/' + `${id}`
  return axios.get(url)
}

export const getUserById = (id) => {
  var url = API_URL + '/' + 'users' + '/' + `${id}`
  return axios.get(url)
}

export const addProduct = (item, token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  return axios.post(API_URL + '/' + 'product', item, config)
}
export const updateProduct = (item, id, token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  return axios.patch(API_URL + '/' + 'product' + '/' + `${id}`, item, config)
}

export const deleteProduct = (id, token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  return axios.delete(API_URL + '/' + 'product' + '/' + `${id}`, config)
}

export const deleteUser = (item, token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  return axios.post(API_URL + '/' + 'manage-user', item, config)
}

export const loginAccount = (item) => {
  const config = {
    headers: { 'Access-Control-Allow-Origin': '*' },
  }
  return axios.post(API_URL + '/' + 'auth' + '/' + 'login', item, config)
}

export const registerAccount = (item) => {
  return axios.post(API_URL + '/' + 'users', item)
}

export const postImage = (item) => {
  return axios.post(API_URL + '/' + 'upload' + '/' + 'one', item)
}

export const AddToCart = (item) => {
  return axios.post(API_URL + '/' + 'cart', item)
}

export const getCommentByProductId = (id) => {
  return axios.get(API_URL + '/' + 'comments' + '/' + 'id' + '/' + id)
}

export const postComment = (item) => {
  return axios.post(API_URL + '/' + 'comments', item)
}

export const replyComment = (item, id) => {
  return axios.post(API_URL + '/' + 'comments' + '/' + `${id}`, item)
}

export const deleteCartById = (productID, userId) => {
  return axios.delete(API_URL + '/' + 'cart' + '/' + 'del' + '/' + `${productID}&${userId}`)
}

export const quantityCartById = (cartId, action) => {
  return axios.patch(API_URL + '/' + 'cart' + '/' + `${cartId}&${action}`)
}

export const checkOutCart = (item) => {
  return axios.post(API_URL + '/' + 'order', item)
}

export const purchaseCart = (id) => {
  return axios.post(API_URL + '/' + 'order' + '/' + `${id}`)
}

export const getOrderHistory = (id) => {
  var url = API_URL + '/' + 'order' + '/' + `${id}`
  return axios.get(url)
}

export const getReport = () => {
  var url = API_URL + '/' + 'report' + '/false&6'
  return axios.get(url)
}

export const getAllOrder = (token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  var url = API_URL + '/' + 'order'
  return axios.get(url, config)
}

export const getAllReport = () => {
  var url = API_URL + '/' + 'management'
  return axios.get(url)
}

export const getAllUser = (token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  var url = API_URL + '/' + 'manage-user'
  return axios.get(url, config)
}

export const postNewAddress = (item) => {
  return axios.post(API_URL + '/' + 'address', item)
}

export const getTopProduct = () => {
  var url = API_URL + '/' + 'products' + '?sortBy=TOPSALES'
  return axios.get(url)
}

export const getProductByKeyword = async ({ keyword }) => {
  try {
    let response = await axios.get(`${API_URL_PRODUCTS}?keyword=${keyword}`)
    if (response.status != 200) {
      throw 'Failed request'
    }
    return response.data.data
  } catch (error) {
    throw error
  }
}

export const getProductDetail = async ({ productID }) => {
  try {
    let response = await axios.get(`${API_URL_PRODUCTS}/${productID}`)
    if (response.status != 200) {
      throw 'Failed request'
    }
    let responseProductDetail = response.data.data
    return responseProductDetail
  } catch (error) {
    throw error
  }
}
