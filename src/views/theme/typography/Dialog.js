import * as React from 'react'
import Button from '@mui/material/Button'
import {
  Dialog,
  DialogTitle,
  IconButton,
  DialogContent,
  DialogActions,
  DialogContentText,
  Box,
} from '@material-ui/core'
import MaterialTable from 'material-table'
export default function AlertDialog({ handleClose, item, open, handleOrder, idOrder }) {
  //   const [open, setOpen] = React.useState(false);

  //   const handleClickOpen = () => {
  //     setOpen(true);
  //   };

  //   const handleClose = () => {
  //     setOpen(false);
  //   };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Order Detail'}</DialogTitle>
      <DialogContent>
        <MaterialTable
          data={item}
          columns={[
            {
              title: 'Image',
              field: '',
              render: (rowData) => {
                return (
                  <Box
                    component="img"
                    src={rowData.product.cover}
                    style={{ objectFit: 'cover', width: 60 }}
                  />
                )
              },
            },
            {
              title: 'Name',
              field: '',
              render: (rowData) => {
                return rowData.product.product_name
              },
            },
            {
              title: 'Quantity',
              field: 'quantity',
              render: (rowData) => {
                return rowData.quantity
              },
            },
            {
              title: 'Price',
              field: 'price',
              render: (rowData) => {
                return parseInt(rowData.price) * rowData.quantity
              },
            },
            // {
            //   title: "Name",
            //   field: "name",
            //   render: (rowData) => {
            //     let result = "";
            //     return <Button onClick={()=> {console.log(rowData)}}>akakkaaa</Button>;
            //   },
            // },
          ]}
          options={{
            actionsColumnIndex: -1,
            paging: false,
            search: false,
            toolbar: false,
            maxBodyHeight: '440px',
            headerStyle: {
              backgroundColor: '#3366ff',
              color: '#fff',
              zIndex: 1,
            },
            rowStyle: (rowData, index) => ({
              backgroundColor: index % 2 === 1 ? 'rgb(237, 245, 251)' : '#FFF',
            }),
            selectionProps: (rowData) => ({
              color: 'primary',
              // backgroundColor: (this.state.selectedItem === rowData.tableData.id) ? '#EEE' : '#FFF'
            }),
          }}
          localization={{
            body: {
              emptyDataSourceMessage: 'No data',
            },
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Close</Button>
        <Button onClick={() => handleOrder(idOrder, 'DONE')} autoFocus>
          Accept
        </Button>
        <Button onClick={() => handleOrder(idOrder, 'CANCELED')} autoFocus>
          Refuse
        </Button>
      </DialogActions>
    </Dialog>
  )
}
