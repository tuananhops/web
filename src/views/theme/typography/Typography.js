import React, { useEffect, useState } from 'react'
import MaterialTable from 'material-table'
import moment from 'moment'
import {
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  TextField,
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  Icon,
  Button,
  Image,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/DeleteOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import CloseIcon from '@material-ui/icons/Close'
import { getReport, getAllOrder } from './service'
import { getAllReport } from './service'
import { updateStatusOrder } from '../colors/service'
import AlertDialog from './Dialog'

const Typography = () => {
  const accessToken = localStorage.getItem('accessToken')
  const [dataTable, setDataTable] = useState([])
  const [openDialog, setOpenDialog] = useState(false)
  const [itemDialog, setItemDialog] = useState()
  const [idOrder, setIdOrder] = useState()

  const handleOrder = (orderId, action) => {
    updateStatusOrder(orderId, action, accessToken).then(() => {
      getAllOrder(accessToken).then((res) => {
        setDataTable(res.data.data)
        setOpenDialog(false)
      })
    })
  }
  useEffect(() => {
    getAllOrder(accessToken).then((res) => {
      console.log('getAllOrder', res.data.data)
      setDataTable(res.data.data)
    })
    // getReport().then((res) => {
    //   console.log('akakakakaa', res.data.data)
    // })
    // getAllReport().then((res) => {
    //   console.log('akakakakaahhhhh', res.data.data)
    //   setDataTable(res.data.data)
    // })
  }, [])

  const handleOpenDialog = (rowData) => {
    setItemDialog(rowData.items)
    setIdOrder(rowData.id)
    console.log('dâdadad', rowData)
    setOpenDialog(true)
    console.log(openDialog)
  }
  return (
    <>
      <div>
        {openDialog && (
          <AlertDialog
            open={openDialog}
            item={itemDialog}
            handleClose={() => setOpenDialog(false)}
            handleOrder={handleOrder}
            idOrder={idOrder}
          />
        )}
        <Grid item xs={12}>
          <MaterialTable
            data={dataTable}
            columns={[
              // {
              //   title: 'Id',
              //   field: 'id',
              //   render: (rowData, index) => {
              //     return index
              //   },
              // },

              {
                title: 'Time',
                field: 'created_at',
                render: (rowData) => {
                  let result = ''
                  if (rowData?.created_at) {
                    result = moment(rowData?.created_at).format('DD/MM/YYYY, h:mm:ss a')
                  }
                  return result
                },
              },
              {
                title: 'Total',
                field: 'total_price',
              },
              {
                title: 'Status',
                field: 'status',
                render: (rowData) => {
                  if (rowData?.status == 'DONE') {
                    return <div style={{ color: 'green' }}>{rowData?.status}</div>
                  } else if (rowData?.status == 'CANCELED') {
                    return <div style={{ color: 'red' }}>{rowData?.status}</div>
                  } else if (rowData?.status == 'PENDING') {
                    return <div style={{ color: 'yellow' }}>{rowData?.status}</div>
                  } else return rowData?.status
                },
              },
              {
                title: 'Actions',
                field: 'custom',
                type: 'numeric',
                // width: "50px",
                render: (rowData) => (
                  <>
                    <IconButton size="small" onClick={() => handleOpenDialog(rowData)}>
                      <EditIcon fontSize="small" color="primary" />
                    </IconButton>

                    {/* <IconButton
                      //   onClick={() => handleDelete(rowData)}
                      size="small"
                    >
                      <DeleteIcon fontSize="small" color="error" />
                    </IconButton> */}
                  </>
                ),
              },
            ]}
            options={{
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              toolbar: false,
              maxBodyHeight: '440px',
              headerStyle: {
                backgroundColor: '#3366ff',
                color: '#fff',
                zIndex: 1,
              },
              rowStyle: (rowData, index) => ({
                backgroundColor: index % 2 === 1 ? 'rgb(237, 245, 251)' : '#FFF',
              }),
              selectionProps: (rowData) => ({
                color: 'primary',
                // backgroundColor: (this.state.selectedItem === rowData.tableData.id) ? '#EEE' : '#FFF'
              }),
            }}
            localization={{
              body: {
                emptyDataSourceMessage: 'No data',
              },
            }}
          />
        </Grid>
      </div>
    </>
  )
}

export default Typography
