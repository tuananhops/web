import React, { useEffect, useState } from 'react'
import MaterialTable from 'material-table'
import moment from 'moment'
import {
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  TextField,
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  Icon,
  Button,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/DeleteOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import CloseIcon from '@material-ui/icons/Close'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import LockIcon from '@material-ui/icons/Lock'
import { getReport } from './service'
import { getAllReport } from './service'
import { getAllUser, deleteUser } from './service'
import { updateStatusOrder } from '../colors/service'
import AlertDialog from './Dialog'

const UserManagement = () => {
  const [dataTable, setDataTable] = useState([])
  const [openDialog, setOpenDialog] = useState(false)
  const [itemDialog, setItemDialog] = useState()
  const [idOrder, setIdOrder] = useState()
  const accessToken = localStorage.getItem('accessToken')

  useEffect(() => {
    getReport().then((res) => {
      console.log('akakakakaa', res.data.data)
    })
    getAllUser(accessToken).then((res) => {
      console.log('akakakakaahhhhh', res.data.data)
      setDataTable(res.data.data)
    })
  }, [])
  const handleDeleteUser = (rowData) => {
    let item = {
      user_id: rowData.id,
      action: 'ban',
    }
    deleteUser(item, accessToken).then(() => {
      getAllUser(accessToken).then((res) => {
        console.log('akakakakaahhhhh', res.data.data)
        setDataTable(res.data.data)
      })
    })
  }
  const handleUnbanUser = (rowData) => {
    let item = {
      user_id: rowData.id,
      action: 'unban',
    }
    deleteUser(item, accessToken).then(() => {
      getAllUser(accessToken).then((res) => {
        console.log('akakakakaahhhhh', res.data.data)
        setDataTable(res.data.data)
      })
    })
  }
  const handleOpenDialog = (rowData) => {
    setItemDialog(rowData.items)
    setIdOrder(rowData.id)
    console.log('dâdadad', rowData)
    setOpenDialog(true)
    console.log(openDialog)
  }
  return (
    <div>
      <>
        <Grid item xs={12}>
          <MaterialTable
            data={dataTable}
            columns={[
              // {
              //   title: "Id",
              //   field: "id",
              //   render: (rowData) => {
              //     return rowData.id;
              //   },
              // },
              {
                title: 'Name',
                field: 'username',
              },
              {
                title: 'Email',
                field: 'email',
              },
              {
                title: 'Role',
                field: 'role',
              },
              {
                title: 'Status',
                field: 'status',
                render: (rowData) => {
                  if (rowData.status) {
                    return <div style={{ color: 'green' }}>UNBAN</div>
                  } else {
                    return <div style={{ color: 'red' }}>BAN</div>
                  }
                },
              },

              {
                title: 'Actions',
                field: 'custom',
                type: 'numeric',
                // width: "50px",
                render: (rowData) => (
                  <>
                    <IconButton onClick={() => handleDeleteUser(rowData)} size="small">
                      <LockIcon fontSize="small" color="error" />
                    </IconButton>
                    <IconButton onClick={() => handleUnbanUser(rowData)} size="small">
                      <LockOpenIcon fontSize="small" style={{ color: 'green' }} />
                    </IconButton>
                  </>
                ),
              },
            ]}
            options={{
              actionsColumnIndex: -1,
              paging: false,
              search: false,
              toolbar: false,
              maxBodyHeight: '440px',
              headerStyle: {
                backgroundColor: '#3366ff',
                color: '#fff',
                zIndex: 1,
              },
              rowStyle: (rowData, index) => ({
                backgroundColor: index % 2 === 1 ? 'rgb(237, 245, 251)' : '#FFF',
              }),
              selectionProps: (rowData) => ({
                color: 'primary',
                // backgroundColor: (this.state.selectedItem === rowData.tableData.id) ? '#EEE' : '#FFF'
              }),
            }}
            localization={{
              body: {
                emptyDataSourceMessage: 'No data',
              },
            }}
          />
        </Grid>
      </>
    </div>
  )
}

export default UserManagement
