import axios from 'axios'

const API_URL = `https://anhkk.site:6060`
const API_URL_PRODUCTS = `https://anhkk.site:6060/products`
const accessToken = localStorage.getItem('accessToken')

export const getAllCategories = (accessToken) => {
  const config = {
    headers: { Authorization: `Bearer ${accessToken}` },
  }
  var url = 'https://anhkk.site:6060/category'
  return axios.get(url, config)
}

export const getProductByCate = (id) => {
  var url = 'https://anhkk.site:6060/categories/id/' + `${id}`
  return axios.get(url)
}
export const getAllProduct = (accessToken) => {
  var url = API_URL + '/' + 'product'
  return axios.get(url, accessToken)
}
export const getUserById = (id) => {
  var url = API_URL + '/' + 'users' + '/' + `${id}`
  return axios.get(url)
}

export const loginAccount = (item) => {
  const config = {
    headers: { 'Access-Control-Allow-Origin': '*' },
  }
  return axios.post(API_URL + '/' + 'auth' + '/' + 'login', item, config)
}

export const registerAccount = (item) => {
  return axios.post(API_URL + '/' + 'users', item)
}

export const AddToCart = (item) => {
  return axios.post(API_URL + '/' + 'cart', item)
}

export const getCommentByProductId = (id) => {
  return axios.get(API_URL + '/' + 'comments' + '/' + 'id' + '/' + id)
}

export const postComment = (item) => {
  return axios.post(API_URL + '/' + 'comments', item)
}

export const replyComment = (item, id) => {
  return axios.post(API_URL + '/' + 'comments' + '/' + `${id}`, item)
}

export const deleteCartById = (productID, userId) => {
  return axios.delete(API_URL + '/' + 'cart' + '/' + 'del' + '/' + `${productID}&${userId}`)
}

export const quantityCartById = (cartId, action) => {
  return axios.patch(API_URL + '/' + 'cart' + '/' + `${cartId}&${action}`)
}

export const updateStatusOrder = (orderId, action, token) => {
  const config = {
    headers: { Authorization: `Bearer ${token}` },
  }
  return axios.patch(API_URL + '/' + 'order' + '/' + `${orderId}`, { action: action }, config)
}

export const checkOutCart = (item) => {
  return axios.post(API_URL + '/' + 'order', item)
}

export const purchaseCart = (id) => {
  return axios.post(API_URL + '/' + 'order' + '/' + `${id}`)
}

export const getOrderHistory = (id) => {
  var url = API_URL + '/' + 'order' + '/' + `${id}`
  return axios.get(url)
}

export const getReport = () => {
  var url = API_URL + '/' + 'report'
  return axios.get(url)
}

export const postNewAddress = (item) => {
  return axios.post(API_URL + '/' + 'address', item)
}

export const getTopProduct = () => {
  var url = API_URL + '/' + 'products' + '?sortBy=TOPSALES'
  return axios.get(url)
}

export const getProductByKeyword = async ({ keyword }) => {
  try {
    let response = await axios.get(`${API_URL_PRODUCTS}?keyword=${keyword}`)
    if (response.status != 200) {
      throw 'Failed request'
    }
    return response.data.data
  } catch (error) {
    throw error
  }
}

export const getProductDetail = async ({ productID }) => {
  try {
    let response = await axios.get(`${API_URL_PRODUCTS}/${productID}`)
    if (response.status != 200) {
      throw 'Failed request'
    }
    let responseProductDetail = response.data.data
    return responseProductDetail
  } catch (error) {
    throw error
  }
}
