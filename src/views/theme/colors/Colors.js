import PropTypes from 'prop-types'
import React, { useEffect, useState, createRef } from 'react'
import classNames from 'classnames'
import { CRow, CCol, CCard, CCardHeader, CCardBody } from '@coreui/react'
import { rgbToHex } from '@coreui/utils'
import { DocsLink } from 'src/components'
import EditDialogProduct from 'src/views/dashboard/EditDialogProduct'
import DeleteIcon from '@material-ui/icons/DeleteOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import {
  IconButton,
  Grid,
  Button,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
} from '@material-ui/core'
import MaterialTable from 'material-table'
import { getAllCategories, getProductByCate, getAllProduct } from './service'
import { deleteProduct } from '../typography/service'

const ThemeView = () => {
  const [color, setColor] = useState('rgb(255, 255, 255)')
  const ref = createRef()

  useEffect(() => {
    const el = ref.current.parentNode.firstChild
    const varColor = window.getComputedStyle(el).getPropertyValue('background-color')
    setColor(varColor)
  }, [ref])

  return (
    <table className="table w-100" ref={ref}>
      <tbody>
        <tr>
          <td className="text-medium-emphasis">HEX:</td>
          <td className="font-weight-bold">{rgbToHex(color)}</td>
        </tr>
        <tr>
          <td className="text-medium-emphasis">RGB:</td>
          <td className="font-weight-bold">{color}</td>
        </tr>
      </tbody>
    </table>
  )
}

const ThemeColor = ({ className, children }) => {
  const classes = classNames(className, 'theme-color w-75 rounded mb-3')
  return (
    <CCol xs={12} sm={6} md={4} xl={2} className="mb-4">
      <div className={classes} style={{ paddingTop: '75%' }}></div>
      {children}
      <ThemeView />
    </CCol>
  )
}
const Colors = () => {
  const [openEdit, setOpenEdit] = useState(false)
  const [rowData, setRowData] = useState(null)
  const [cate, setCate] = useState([])
  const [productPerCate, setProductPerCate] = useState([])
  const [cateValue, setCateValue] = useState({})
  const accessToken = localStorage.getItem('accessToken')
  useEffect(() => {
    getAllCategories(accessToken).then((res) => {
      setCate(res.data)
      console.log('akakakakaa', res.data.data)
    })
    getAllProduct(accessToken).then((res) => {
      console.log(res.data.data)
      setProductPerCate(res.data.data)
    })
  }, [])
  useEffect(() => {
    if (!openEdit) {
      getAllProduct(accessToken).then((res) => {
        console.log(res.data.data)
        setProductPerCate(res.data.data)
      })
    }
  }, [openEdit])
  const handleOpenEdit = (rowData) => {
    setOpenEdit(true)
    setRowData(rowData)
  }
  const handleDelete = (rowData) => {
    deleteProduct(rowData.id, accessToken).then(() => {
      getAllProduct(accessToken).then((res) => {
        console.log(res.data.data)
        setProductPerCate(res.data.data)
      })
    })
  }
  const handleChange = (value) => {
    setCateValue(value)
    console.log(value)
    getProductByCate(value?.id).then((res) => {
      console.log('datatable', res.data.data)
      setProductPerCate(res.data.data?.products)
    })
  }

  return (
    <>
      <div>
        {openEdit && (
          <EditDialogProduct openEdit={openEdit} setOpenEdit={setOpenEdit} rowData={rowData} />
        )}
        <Grid container spacing={2}>
          <Grid item xs={12} md={12}>
            <FormControl variant="standard" style={{ width: '300px' }}>
              <InputLabel id="demo-simple-select-standard-label"></InputLabel>
              {/* <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={cateValue}
                onChange={(event) => handleChange(event.target.value)}
                label="Category"
              >
                {cate?.map((data) => {
                  return (
                    <MenuItem key={data?.id} value={data}>
                      {data?.name}
                    </MenuItem>
                  )
                })}
              </Select> */}
            </FormControl>
          </Grid>
          <Grid item xs={12} md={12}>
            <Button onClick={() => handleOpenEdit({})}>Add Product</Button>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <MaterialTable
              data={productPerCate ? productPerCate : []}
              columns={[
                {
                  title: 'Image',
                  field: 'custom',
                  render: (rowData) => {
                    let result = ''
                    if (rowData?.cover) {
                      result = (
                        <img
                          src={rowData?.cover}
                          style={{
                            width: '80px',
                            height: '80px',
                            objectFit: 'contain',
                          }}
                        ></img>
                      )
                    }
                    return result
                  },
                },
                {
                  title: 'Name',
                  field: 'product_name',
                },
                {
                  title: 'Price',
                  field: 'custom',
                  render: (rowData) => {
                    let result = ''
                    if (rowData?.price) {
                      result = `${rowData?.price}$`
                    }
                    return result
                  },
                },

                // {
                //   title: "Time",
                //   field: "day",
                //   render: (rowData) => {
                //     let result = "";
                //     if (rowData?.day) {
                //       result = moment
                //         .unix(rowData?.day)
                //         .format("DD/MM/YYYY, h:mm:ss a");
                //     }
                //     return result;
                //   },
                // },
                // {
                //   title: "Total",
                //   field: "total",
                // },
                {
                  title: 'Actions',
                  field: 'custom',
                  type: 'numeric',
                  // width: "50px",
                  render: (rowData) => (
                    <>
                      <IconButton size="small" onClick={() => handleOpenEdit(rowData)}>
                        <EditIcon fontSize="small" color="primary" />
                      </IconButton>

                      <IconButton onClick={() => handleDelete(rowData)} size="small">
                        <DeleteIcon fontSize="small" color="error" />
                      </IconButton>
                    </>
                  ),
                },
              ]}
              options={{
                actionsColumnIndex: -1,
                paging: false,
                search: false,
                toolbar: false,
                maxBodyHeight: '420px',
                headerStyle: {
                  backgroundColor: '#3366ff',
                  color: '#fff',
                  zIndex: 1,
                },
                rowStyle: (rowData, index) => ({
                  backgroundColor: index % 2 === 1 ? 'rgb(237, 245, 251)' : '#FFF',
                }),
                selectionProps: (rowData) => ({
                  color: 'primary',
                  // backgroundColor: (this.state.selectedItem === rowData.tableData.id) ? '#EEE' : '#FFF'
                }),
              }}
              localization={{
                body: {
                  emptyDataSourceMessage: 'No data',
                },
              }}
            />
          </Grid>
        </Grid>
      </div>
    </>
  )
}

export default Colors
