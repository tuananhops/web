import React, { useEffect, useState } from 'react'
import MaterialTable from 'material-table'
import moment from 'moment'
import {
  Box,
  InputLabel,
  MenuItem,
  Select,
  FormControl,
  TextField,
  Grid,
  IconButton,
  Dialog,
  DialogTitle,
  Icon,
  Button,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/DeleteOutlined'
import EditIcon from '@material-ui/icons/EditOutlined'
import CloseIcon from '@material-ui/icons/Close'
import { postImage } from '../theme/typography/service'
import { getAllCategories } from '../theme/typography/service'
import { addProduct } from '../theme/typography/service'
import { updateProduct } from '../theme/typography/service'

function EditDialogProduct({ openEdit, setOpenEdit, rowData }) {
  const [idProduct, setIdProduct] = useState()
  const [categoryProduct, setCategoryProduct] = useState(rowData.category_id)
  const [priceProduct, setPriceProduct] = useState(rowData.price)
  const [nameProduct, setNameProduct] = useState(rowData.product_name)
  const [descriptionProduct, setDescriptionProduct] = useState(rowData.description)
  const [stockQuantity, setStockQuantity] = useState(rowData.stock)
  const [image, setImage] = useState(null)
  const [url, setUrl] = useState(rowData.cover)
  const [listOptionCate, setListOptionCate] = useState([])
  const [imageSum, setImageSum] = useState()
  const accessToken = localStorage.getItem('accessToken')
  useEffect(() => {
    setImageSum([{}])
    getAllCategories(accessToken).then((res) => {
      setListOptionCate(res.data.data)
      console.log('ahihhihihgg', res.data)
    })
    console.log('sahdgadasgdamm', rowData)
    if (rowData.cover && rowData.images) {
      let arrayImage = rowData.images
      // arrayImage.splice(0, 0, rowData.cover);
      setImageSum(arrayImage)
      console.log('dshasdgashdakakaaka', arrayImage)
    }
  }, [])
  const handleChange = (event, index) => {
    const newImageSum = [...imageSum]
    console.log('event', event)
    const fd = new FormData()
    fd.append('file', event.target.files[0], event.target.files[0].name)
    postImage(fd).then((res) => {
      console.log('aksda', res)
      newImageSum.splice(index, 1, res.data.data)
      setImageSum(newImageSum)
      console.log('ahuhuhuhihi', imageSum)
    })
  }
  const addImage = () => {
    const newImageSum = [...imageSum]
    newImageSum.push({})
    setImageSum(newImageSum)
    console.log(imageSum)
  }

  const deleteImage = (index) => {
    const newImageSum = [...imageSum]
    newImageSum.splice(index, 1)
    setImageSum(newImageSum)
  }

  const handleAddProduct = () => {
    const item = {
      product_name: nameProduct,
      sku: 'IPA11',
      cover: imageSum[1],
      images: imageSum,
      description: descriptionProduct,
      price: parseInt(priceProduct),
      stock: parseInt(stockQuantity),
      category_id: categoryProduct,
    }
    addProduct(item, accessToken).then((res) => {
      console.log('muaksdahdfsafgd', res)
      setOpenEdit(false)
    })
  }

  const handleUpdateProduct = () => {
    const item = {
      product_name: nameProduct,
      sku: 'IPA11',
      cover: imageSum[1],
      images: imageSum,
      description: descriptionProduct,
      price: parseInt(priceProduct),
      stock: parseInt(stockQuantity),
      category_id: categoryProduct,
    }
    updateProduct(item, rowData.id, accessToken).then((res) => {
      console.log('muaksdahdfsafgd', res)
      setOpenEdit(false)
    })
  }

  return (
    <Dialog className="dialog-container" open={openEdit} maxWidth="lg" fullScreen>
      <DialogTitle>
        <span>{rowData.id ? `Edit Product` : 'Add Product'}</span>
        <IconButton
          style={{ position: 'absolute', right: '3px', top: '3px' }}
          onClick={() => setOpenEdit(false)}
        >
          <CloseIcon fontSize="large" color="disabled" />
        </IconButton>
      </DialogTitle>
      <div style={{ width: '95%' }}>
        <Grid container spacing={2} justify="center">
          <Grid item lg={9} md={9}>
            <TextField
              fullWidth
              label="Product Name"
              value={nameProduct}
              onChange={(e) => setNameProduct(e.target.value)}
            />
          </Grid>
          <Grid item lg={9} md={9}>
            <TextField
              fullWidth
              label="Price"
              value={priceProduct}
              onChange={(e) => setPriceProduct(e.target.value)}
            />
          </Grid>
          <Grid item lg={9} md={9}>
            <TextField
              fullWidth
              label="Description"
              value={descriptionProduct}
              onChange={(e) => setDescriptionProduct(e.target.value)}
            />
          </Grid>
          <Grid item lg={9} md={9}>
            <TextField
              fullWidth
              label="Stock"
              value={stockQuantity}
              onChange={(e) => setStockQuantity(e.target.value)}
            />
          </Grid>

          <Grid item lg={9} md={9}>
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={categoryProduct}
                  label="Category"
                  onChange={(e) => setCategoryProduct(e.target.value)}
                >
                  {listOptionCate.map((data) => {
                    return (
                      <MenuItem key={data.id} value={data.id}>
                        {data.category_name}
                      </MenuItem>
                    )
                  })}
                  {/* <MenuItem value={"deal"}>Deal</MenuItem>
                  <MenuItem value={"laptop"}>Laptop</MenuItem>
                  <MenuItem value={"bed"}>Bed</MenuItem>
                  <MenuItem value={"oculus"}>Oculus</MenuItem>
                  <MenuItem value={"tv"}>TV</MenuItem>
                  <MenuItem value={"sport"}>Sport</MenuItem>
                  <MenuItem value={"beauty"}>Beauty</MenuItem>
                  <MenuItem value={"amabasic"}>Amabasic</MenuItem>
                  <MenuItem value={"bed"}>Bed</MenuItem>
                  <MenuItem value={"electronic"}>Electronic</MenuItem> */}
                </Select>
              </FormControl>
            </Box>
          </Grid>
          <Grid item lg={9} md={9}></Grid>
          {imageSum &&
            imageSum.map((data, index) => {
              return (
                <Grid item lg={9} md={9} key={index}>
                  <div>
                    <input type="file" onChange={(event) => handleChange(event, index)} />
                    {/* <input type="button" placeholder="delete image" onClick={() => deleteImage(index)}/> */}

                    <img
                      style={{ height: '300px', width: '300px', objectFit: 'contain' }}
                      src={
                        typeof imageSum[index] == 'string'
                          ? imageSum[index]
                          : 'http://via.placeholder.com/300'
                      }
                      alt="firebase-image"
                    />
                  </div>
                </Grid>
              )
            })}
          <Grid item lg={9} md={9}>
            <Button onClick={addImage}>Add Image</Button>
          </Grid>
          <Grid item lg={11} md={11} style={{ justifyContent: 'center', display: 'flex' }}>
            {!rowData.product_name ? (
              <Button style={{ width: '90px' }} onClick={() => handleAddProduct()}>
                Send
              </Button>
            ) : (
              <Button onClick={() => handleUpdateProduct()}>Update</Button>
            )}
          </Grid>
        </Grid>
      </div>
    </Dialog>
  )
}

export default EditDialogProduct
