import React, { useEffect, useState } from "react";

import {
  CAvatar,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import { CChartLine } from '@coreui/react-chartjs'
import { getStyle, hexToRgba } from '@coreui/utils'
import CIcon from '@coreui/icons-react'
import {
  cibCcAmex,
  cibCcApplePay,
  cibCcMastercard,
  cibCcPaypal,
  cibCcStripe,
  cibCcVisa,
  cibGoogle,
  cibFacebook,
  cibLinkedin,
  cifBr,
  cifEs,
  cifFr,
  cifIn,
  cifPl,
  cifUs,
  cibTwitter,
  cilCloudDownload,
  cilPeople,
  cilUser,
  cilUserFemale,
} from '@coreui/icons'

import avatar1 from 'src/assets/images/avatars/1.jpg'
import avatar2 from 'src/assets/images/avatars/2.jpg'
import avatar3 from 'src/assets/images/avatars/3.jpg'
import avatar4 from 'src/assets/images/avatars/4.jpg'
import avatar5 from 'src/assets/images/avatars/5.jpg'
import avatar6 from 'src/assets/images/avatars/6.jpg'
import { getReport } from '../theme/typography/service'
import WidgetsBrand from '../widgets/WidgetsBrand'
import WidgetsDropdown from '../widgets/WidgetsDropdown'

const Dashboard = () => {
  const random = (min, max) => Math.floor(Math.random() * (max - min + 1) + min)
  const [dataForChart,setDataForChart] = useState([0,0,0,0,0,0,0,0,0,0,0,0])
  useEffect(() => {
getReport().then(res =>{
  console.log("sahdashdasgda",res.data.data)
setDataForChart(res.data.data.overview)
})
  }, [])

  return (
    <>
      <CCard className="mb-4">
        <CCardBody>
          <CChartLine
            style={{ height: '300px', marginTop: '40px' }}
            data={{
              labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
              datasets: [
                {
                  label: 'Number of product sold',
                  backgroundColor: hexToRgba(getStyle('--cui-info'), 10),
                  borderColor: getStyle('--cui-info'),
                  pointHoverBackgroundColor: getStyle('--cui-info'),
                  borderWidth: 2,
                  data: [
                    dataForChart[0].numOfProductSold,
                    dataForChart[1].numOfProductSold,
                    dataForChart[2].numOfProductSold,
                    dataForChart[3].numOfProductSold,
                    dataForChart[4].numOfProductSold,
                    dataForChart[5].numOfProductSold,
                    dataForChart[6].numOfProductSold,
                    dataForChart[7].numOfProductSold,
                    dataForChart[8].numOfProductSold,
                    dataForChart[9].numOfProductSold,
                    dataForChart[10].numOfProductSold,
                    dataForChart[11].numOfProductSold
                  ],
                  fill: true,
                },
                {
                  label: 'Total money',
                  backgroundColor: 'transparent',
                  borderColor: getStyle('--cui-success'),
                  pointHoverBackgroundColor: getStyle('--cui-success'),
                  borderWidth: 2,
                  data: [
                    dataForChart[0].totalMoney,
                    dataForChart[1].totalMoney,
                    dataForChart[2].totalMoney,
                    dataForChart[3].totalMoney,
                    dataForChart[4].totalMoney,
                    dataForChart[5].totalMoney,
                    dataForChart[6].totalMoney,
                    dataForChart[7].totalMoney,
                    dataForChart[8].totalMoney,
                    dataForChart[9].totalMoney,
                    dataForChart[10].totalMoney,
                    dataForChart[11].totalMoney
                  ],
                },
                // {
                //   label: 'My Third dataset',
                //   backgroundColor: 'transparent',
                //   borderColor: getStyle('--cui-danger'),
                //   pointHoverBackgroundColor: getStyle('--cui-danger'),
                //   borderWidth: 1,
                //   borderDash: [8, 5],
                //   data: [65, 65, 65, 65, 65, 65, 65],
                // },
              ],
            }}
            options={{
              maintainAspectRatio: false,
              plugins: {
                legend: {
                  display: false,
                },
              },
              scales: {
                x: {
                  grid: {
                    drawOnChartArea: false,
                  },
                },
                y: {
                  ticks: {
                    beginAtZero: true,
                    maxTicksLimit: 5,
                    stepSize: Math.ceil(250 / 5),
                    max: 250,
                  },
                },
              },
              elements: {
                line: {
                  tension: 0.4,
                },
                point: {
                  radius: 0,
                  hitRadius: 10,
                  hoverRadius: 4,
                  hoverBorderWidth: 3,
                },
              },
            }}
          />
        </CCardBody>
      </CCard>
    </>
  )
}

export default Dashboard
